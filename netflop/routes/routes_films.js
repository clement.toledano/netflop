var express = require('express');
var router = express.Router();

var controller_films = require('../controllers/controller_films');


router.get('/', controller_films.index)


router.get('/film?:id', controller_films.film_details);
router.post('/add', controller_films.addNote);


module.exports = router;