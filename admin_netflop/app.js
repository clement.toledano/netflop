var createError = require('http-errors');
var express = require('express');
var path = require('path');
//var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cons = require('consolidate');
var session = require('express-session')



var adminRouter = require('./routes/routeadmin');
var registerRouter = require('./routes/routeregister');


var app = express();

// view engine setup
app.engine('html', cons.swig)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//app.use(cookieParser());
app.use(session({secret:"dfg51fd5s514",resave:false,saveUninitialized:true}))
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', adminRouter);
app.use('/login', adminRouter);
app.use('/users', registerRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('404');
});

module.exports = app;
